#!/usr/bin/python
import logging

def setup_logger(name):
	formatter = logging.Formatter('%(asctime)s-%(levelname)s-%(module)s-%(threadName)s-%(message)s')
	handler = logging.FileHandler('/var/log/rfm_gateway.log')
	#handler = logging.FileHandler('rfm_gateway.log')
	handler.setFormatter(formatter)
	logger = logging.getLogger(name)
	logger.setLevel(logging.WARNING)
	#logger.setLevel(logging.DEBUG)
	#logger.setLevel(logging.INFO)
	logger.addHandler(handler)
	logger.info("Library logs.py started...")
	return logger

if __name__ == "__main__":
	print("This module is not executable, please run rfm_gateway.py instead")

