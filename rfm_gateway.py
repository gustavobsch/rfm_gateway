#!/usr/bin/python
import ConfigParser, os, utils, time, datetime, logs, socket, threading, sys, pytz, ast, json, serial, base64, socket, binascii
from datetime import datetime
import paho.mqtt.client as mqtt

starttime = time.time()

logger = logs.setup_logger('rfm_gateway')
logger.warning("Program Started...")

### Get run mode
if len(sys.argv) > 1:
	runmode = sys.argv[1]
elif len(sys.argv) == 1:
	runmode = 'normal'

### This thread maintains envriomental variables and restarts program's threads in case they crash for any reason
class supervisor(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		while True:
			try:
				# Start MQTT thread if none running
				if "mqtt_thread" not in get_threads():
					thread = mqtt_connect(name = 'mqtt_thread')
					thread.daemon = True
					thread.start()
					logger.warning("Restarting mqtt_connect thread...")
				# Start read_xbee_frame threads if not already running, one per xbee modem configured
				for rfm_transceiver_id, attributes in transceiver_rfs.iteritems():
					if attributes['transceiver_type'] == 'rfm915' and str(rfm_transceiver_id)+"_gateway_parse_rfm95_frame_thread" not in get_threads():
						thread_name = str(rfm_transceiver_id)+"_gateway_parse_rfm95_frame_thread"
						thread = parse_rfm95_frame(rfm_transceiver_id, thread_name)
						thread.daemon = True
						thread.start()
						logger.warning("Starting a parse_rfm95_frame thread for rfm transceiver '%s' ..." % rfm_transceiver_id)
					if attributes['transceiver_type'] == 'rfm96w' and str(rfm_transceiver_id)+"_gateway_parse_rfm96w_frame_thread" not in get_threads():
						thread_name = str(rfm_transceiver_id)+"_gateway_parse_rfm96w_frame_thread"
						thread = parse_rfm96w_frame(rfm_transceiver_id, thread_name)
						thread.daemon = True
						thread.start()
						logger.warning("Starting a parse_rfm96w_frame thread for rfm transceiver '%s' ..." % rfm_transceiver_id)
				time.sleep(1)
			except Exception:
				sys.excepthook(*sys.exc_info())
			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print "Other error or exception occurred!"

class parse_rfm96w_frame(threading.Thread):
	def __init__(self, rfm_transceiver_id, name = None):
		threading.Thread.__init__(self, name = name)
		self.id = rfm_transceiver_id
		self.serial = transceiver_rfs[self.id]['serial']
		self.latitude = config['gateway_latitude']
		self.longitude = config['gateway_longitude']
		self.altitude = config['gateway_altitude']
		self.mac = config['gateway_mac']
		self.server_ip = socket.getaddrinfo(transceiver_rfs[self.id]['gateway_hostname'], transceiver_rfs[self.id]['gateway_port'])[0][-1]
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.setblocking(False)
		self.rxnb = 0
		self.rxok = 0
		self.rxfw = 0
		self.dwnb = 0
		self.txnb = 0
		 # create stat alarms
		#self.push_stats = threading.Timer(2.0, self._push_data, [self._make_stat_packet]).start()
		#self.push_stats.daemon = True
		#self.push_stats.start()
		return
	def _make_stat_packet(self):
		now = datetime.now().strftime('%Y-%m-%d %H:%M:%S GMT')
		stat_pk = {
			'stat': {
				'time': now,
				'lati': self.latitude,
				'long': self.longitude,
				'alti': self.altitude,
				'rxnb': self.rxnb,
				'rxok': self.rxok,
				'rxfw': self.rxfw,
				'ackr': 100.0,
				'dwnb': self.dwnb,
				'txnb': self.txnb
			}
		}
		return json.dumps(stat_pk)
	def _make_node_packet(self, frame_payload, rssi):
		time_delta = datetime.now() - datetime(1980, 1, 6, 0, 0)
		tmst = int(time_delta.total_seconds())
		now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ')
		rxpk = {
			'rxpk': [{
				'time': now,
				'tmst': tmst,
				'chan': int(transceiver_rfs[self.id]['channel']),
				'rfch': int(transceiver_rfs[self.id]['rfchain']),
				'freq': float(transceiver_rfs[self.id]['frequency']),
				'stat': 1,
				'modu': transceiver_rfs[self.id]['modulation'],
				'datr': transceiver_rfs[self.id]['datarate'],
				'codr': transceiver_rfs[self.id]['coderate'],
				'rssi': int(rssi),
				'lsnr': float(transceiver_rfs[self.id]['lorasnr']),
				'size': utils.size(frame_payload),
				'data': frame_payload
			}]}
		return json.dumps(rxpk)
	def _push_data(self, data):
		token = os.urandom(2)
		packet = binascii.unhexlify('02') + token + binascii.unhexlify('00') + binascii.unhexlify(self.mac) + data
		try:
			self.sock.sendto(packet, self.server_ip)
		except Exception as ex:
			logger.exception('Failed to push uplink packet to server: {}', ex)
	def run(self):
		while True:
			try:
				frame_payload = self.serial.readline()
				if len(frame_payload) > 0:
					if runmode == 'debug':
						print "##### ##### DEBUG %s ##### ##### #####" % self.name
						print "Parsing %s" % frame_payload
					self.rxnb = 1
					self.rxok = 1
					'''for i, c in enumerate(frame_payload):
						print i, c
						if c == ',':
							payload_end = i'''
					rssi = utils.parse_rssi(frame_payload[0:3])
					frame_payload = base64.b64encode(frame_payload[3:])
					if runmode == 'debug':
						print "Received frame with base64 payload '%s' and rssi '%s'" % (frame_payload, rssi)
						print "##### END #####"
					### send udp payload
					self._push_data(self._make_node_packet(frame_payload, rssi))
					self._push_data(self._make_stat_packet())
			except Exception:
				sys.excepthook(*sys.exc_info())
			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print("Other error or exception occurred!")

class parse_rfm95_frame(threading.Thread):
	def __init__(self, rfm_transceiver_id, name = None):
		threading.Thread.__init__(self, name = name)
		self.id = rfm_transceiver_id
		self.serial = transceiver_rfs[self.id]['serial']
		self.latitude = config['gateway_latitude']
		self.longitude = config['gateway_longitude']
		self.altitude = config['gateway_altitude']
		self.mac = config['gateway_mac']
		self.modulation = transceiver_rfs[self.id]['modulation']
		self.bandwidth = transceiver_rfs[self.id]['bandwidth']
		self.frequency = transceiver_rfs[self.id]['frequency']
		self.fcnt = 0
		### Initiate containers for MQTT payload here
		self.payload = {'txInfo':{'frequency':int(self.frequency), 'dataRate':{'modulation':self.modulation, 'bandwidth':int(self.bandwidth)}}}
		return
	def run(self):
		while True:
			try:
				frame_payload = self.serial.readline()
				if len(frame_payload) > 0 and utils.parse_payload(frame_payload):
					self.fcnt += 1
					frame_payload = utils.parse_payload(frame_payload)
					senderid = frame_payload['deviceid']
					if senderid < 10:
						senderid = '0'+str(format(senderid, 'x'))
					else:
						senderid = str(format(senderid, 'x'))
					devicename = 'lora'+str(frame_payload['deviceid'])
					deveui = self.mac[0:14]+senderid
					rssi = frame_payload['rssi']
					frame_payload['deviceid'] = devicename
					if runmode == 'debug':
						print "##### ##### DEBUG %s ##### ##### #####" % self.name
						print "Received %s from %s with rssi %s" % (frame_payload, deveui, rssi)
						print "##### END #####"
					# Add RSSI and other rxinfo parameters
					rxinfo = {'rxInfo':[{'mac':str(self.mac),'latitude':float(self.latitude),'longitude':float(self.longitude),'altitude':int(self.altitude), 'rssi':rssi}]}
					self.payload.update({'object': frame_payload})
					self.payload.update(rxinfo)
					self.payload.update({'deviceName':devicename,'devEUI':deveui})
					self.payload.update({'fCnt':self.fcnt})
					print self.payload
					publish_mqtt(self.payload)
			except Exception:
				sys.excepthook(*sys.exc_info())
				break
			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print "Other error or exception occurred!"

def get_threads():
	threads = []
	main_thread = threading.current_thread()
	for t in threading.enumerate():
		if t is main_thread:
			continue
			print t.getName()
		threads.append(t.getName())
	return threads
	
def config_load():
	global config
	global transceivers
	config = {}
	transceivers = {}
	config_file = ConfigParser.RawConfigParser()
	config_file.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'config.cfg'))
	config.update({
	'gateway_id':utils.chop_comment(config_file.get('gateway', 'id')),
	'gateway_latitude':float(utils.chop_comment(config_file.get('gateway', 'latitude'))),
	'gateway_longitude':float(utils.chop_comment(config_file.get('gateway', 'longitude'))),
	'gateway_altitude':int(utils.chop_comment(config_file.get('gateway', 'altitude'))),
	'gateway_mac':utils.chop_comment(config_file.get('gateway', 'mac')),
	'mqtt_broker_hostname':utils.chop_comment(config_file.get('mqtt_broker', 'hostname')),
	'mqtt_broker_port':int(utils.chop_comment(config_file.get('mqtt_broker', 'port'))),
	'mqtt_broker_username':utils.chop_comment(config_file.get('mqtt_broker', 'username')),
	'mqtt_broker_pass':utils.chop_comment(config_file.get('mqtt_broker', 'password')),
	'mqtt_publish_topic':utils.chop_comment(config_file.get('mqtt_broker', 'publish_topic')),
	'mqtt_subscribe_topic':utils.chop_comment(config_file.get('mqtt_broker', 'subscribe_topic')),
	'timezone':config_file.get('environment', 'timezone'),
	})
	for section in config_file.sections():
		if section.startswith('transceiver'):
			for key, value in config_file.items(section):
				if key == 'transceiver_id':
					transceiver_id = value
					transceivers.update({transceiver_id:{'settings':{}}})
					for key, value in config_file.items(section):
						if key == 'transceiver_id':
							continue
						else:
							value = utils.chop_comment(value)
							transceivers[transceiver_id]['settings'].update({key:value})

def transceiver_load():
	global transceiver_rfs
	transceiver_rfs = {}
	parameters = {}
	for transceiver, attributes in transceivers.iteritems():
		for parameter, value in attributes['settings'].iteritems():
			transceiver_rfs.update({transceiver:{}})
			if parameter == 'protocol' and value == 'raw':
				ser = serial.Serial(attributes['settings']['port'], attributes['settings']['baudrate'], timeout=1)
			elif parameter == 'protocol' and value == 'rfc2217':
				ser = serial.serial_for_url(attributes['settings']['port'], attributes['settings']['baudrate'], timeout=1)
			else:
				parameters.update({parameter:value})
		transceiver_rfs[transceiver].update({'serial':ser})
		transceiver_rfs[transceiver].update(parameters)
	print transceiver_rfs

### MQTT funcionts here
def on_connect(client, userdata, flags, rc):
	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	if rc == 0:
		logger.warning("Succesfully connected to MQTT broker, starting subscriptions if any.")
		# Start Subscriptions here
		client.subscribe(config['mqtt_subscribe_topic'], 1)
		logger.warning("Subscribed to topic '%s'" % config['mqtt_subscribe_topic'])
	else:
		logger.warning("Connection to MQTT broker failed. rc='%s' " % s)

def on_publish(client, userdata, mid):
	logger.debug("PUBLISH '%s' completed." % mid)

def on_message(client, userdata, msg):
	if msg.topic == config['mqtt_subscribe_topic']:
		logger.info("Processing Publish '%s'." % msg.payload)
		# First convert payload to a dictionary

def on_disconnect(client, userdata, rc):
	if rc != 0:
		logger.warning("Unexpected MQTT Broker disconnection. Will auto-reconnect.")

def on_log(mosq, obj, level, string):
	logger.debug(string)

def publish_mqtt(payload):
	from datetime import datetime
	### Add timestamp
	now = datetime.now(pytz.timezone(config['timezone'])).strftime('%Y-%m-%d %H:%M:%S')
	rxinfo = payload['rxInfo'][0]
	rxinfo.update({'time':now})
	del payload['rxInfo']
	payload.update({'rxInfo':[rxinfo]})
	# Pass the container to the MQTT thread for publishing
	mqttc.publish('%s/%s/rx' % (config['mqtt_publish_topic'], config['gateway_mac']), json.dumps(payload))
	if runmode == 'debug':
		print "# MQTT: '%s/%s %s'" % (config['mqtt_publish_topic'], config['gateway_mac'], payload)
		print "##### END #####"

### Classes / functions start here
class mqtt_connect(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		global mqttc
		mqttc = mqtt.Client(config['gateway_id'])
		mqttc.username_pw_set(config['mqtt_broker_pass'], config['mqtt_broker_pass'])
		mqttc.connect(config['mqtt_broker_hostname'], config['mqtt_broker_port'], 80)
		mqttc.on_connect = on_connect
		#mqttc.on_message = on_message
		mqttc.on_publish = on_publish
		mqttc.on_disconnect = on_disconnect
		# Uncomment here to enable MQTT debug messages
		#mqttc.on_log = on_log
		while True:
			mqttc.loop()

def check_network():
	loop = 1
        while loop == 1:
		try:
 			s = socket.create_connection((socket.gethostbyname(config['mqtt_broker_hostname']), config['mqtt_broker_port']), 2)
			logger.warning("Network checks passed!, '%s' is reachable." % config['mqtt_broker_hostname'])
			loop = 0
		except Exception:
			logger.warning("Network checks failed!, stalling until MQTT Broker '%s' becomes reachable..." % config['mqtt_broker_hostname'])
			time.sleep(15)
			
if __name__ == "__main__":
	print "rfm_gateway is spinning. Check /var/log/rfm_gateway.log for more details..."
	# Load environment variables and config parameters from file config.cfg
	logger.warning("Loading configuration parameters from config file.")
	config_load()
	# Perfrom network checks and wait there until we can reach the MQTT broker
	logger.warning("Connecting to MQTT Broker.")
	check_network()
	# Create xbee transceiver objects
	logger.warning("Loading tranceivers.")
	transceiver_load()
	try:
		# Start MQTT thread here
		thread = mqtt_connect(name = 'mqtt_thread')
		thread.daemon = True
		thread.start()
		logger.warning("Starting mqtt_connect thread...")
		# Supervisor thread starts last
		thread = supervisor(name = 'supervisor')
		thread.daemon = True
		thread.start()
		logger.warning("Started supervisor thread...")
		if runmode == 'debug':
			while True:
				print("Number of active threads: %s" % threading.active_count())
				print(get_threads())
				time.sleep(30.0 - ((time.time() - starttime) % 30.0))

	except KeyboardInterrupt:
		logger.exception("Got exception on main handler:")
		if runmode == 'debug':
			print "Got exception on main handler:"
		mqttc.loop_stop()
		mqttc.disconnect()
		raise

	except:
		logger.exception("Other error or exception occurred!")
		if runmode == 'debug':
			print "Other error or exception occurred!"
		mqttc.loop_stop()
		mqttc.disconnect()
		for transceiver, settings in transceiver_rfs.iteritems():
			attributes['serial'].close()

	while True:
		time.sleep(1)
		pass










